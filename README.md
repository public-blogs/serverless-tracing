# Serverless Tracing Example Code

This project contains a SAM Template and application code for the below architecture:

![image info](./Serverless_Tracing.png)

It is intended to be used as an example application to show how AWS X-Ray can be used to help to:
  - Increase Observability of Serverless Applications in AWS
  - Simplify the process of identifying the source of errors in serverless applications
  - Help to identify bottlenecks and sources of high latency in serverless applications.

  The application consists of an API Gateway with 2 endpoints.  Each endpoint invokes a Lambda function.  

  The intended use is to make an HTTP PUT request to the orders endpoint.  
  
  This will invoke the Put_Order function, which in turn makes an HTTP GET request to the customer endpoint.

  The customer endpoint invokes the Get_Customer Lambda function, which retrieves a record from the CustomerDB and returns a response to the Put_Order function.

  Finally, the Put_Order function writes a record to the OrderDB.

  Every step of this process in recorded, along with request metadata, in X-Ray and so this should be easy to visualize using the X-Ray Service Map view.

## Prerequisites 
To use provision the application the following prerequisites must be satisfied:
  - The AWS CLI (https://aws.amazon.com/cli/) is installed and configured.
  - The AWS Serverless Application Model (SAM) CLI tools are installed (https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html).
  - Docker (https://docs.docker.com/get-docker/) is installed.
## Getting Started
To get started simply run the following commands.
 1. ```sam build --use-container```
 2. ```sam deploy --guided``` and follow the CLI prompts