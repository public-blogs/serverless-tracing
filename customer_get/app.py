import json
import os
import requests
import boto3
from random import randrange
import botocore

from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.core import patch_all

patch_all()

TABLE_NAME = os.environ["TableName"]

def lambda_handler(event, context):

    customer_response = get_customer()

    item_found = "Item" in customer_response.keys()

    if item_found:
        response_code = customer_response["ResponseMetadata"]["HTTPStatusCode"]
        print("Response Code: " + str(response_code))
    else:
        response_code = 404
        print("Response Code: " + str(response_code))


    return {
        "statusCode": response_code,
        "body": json.dumps({
            "item": customer_response["Item"] if item_found else ""
        }),
    }

def get_customer():
    dynamodb = boto3.client('dynamodb')

    try:
        response = dynamodb.get_item(
            TableName="CustomersDynamoTableName",
            Key={
                'CustomerId': {
                    'S': '456'
                }
            }
        )
    except Exception as e:
        print("GET CUSTOMER - EXCEPTION OCCURED: " + e)
        raise Exception("Failed to Get Customer")


    print("GET CUSTOMER Response: " + str(response))

    return response
