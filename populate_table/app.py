import boto3
import os
import cfnresponse

def lambda_handler(event, context):
    dynamodb = boto3.client('dynamodb')

    dynamo_response = dynamodb.put_item(
        TableName=event["ResourceProperties"]["DynamoTableName"],
        Item={
           'CustomerId':{
               'S': '456'
           },
           'Address':{
               'S': 'placeholder'
           }
        }
    )

    print(dynamo_response)

    if str(dynamo_response['ResponseMetadata']['HTTPStatusCode'] == "200"):
        cfnresponse.send(event, context, cfnresponse.SUCCESS, {})
    else:
        cfnresponse.send(event, context, cfnresponse.FAILED, {})