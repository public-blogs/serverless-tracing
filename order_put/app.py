import json
import os
import boto3
import requests
from random import randrange
import botocore
import time

from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.core import patch_all

patch_all()

TABLE_NAME = os.environ["TableName"]

def lambda_handler(event, context):
    time.sleep(5)

    request_context = event['requestContext']

    domainName = request_context['domainName']

    url = "https://" + domainName + "/Dev/customer"

    get_customer_response = get_customer(url)

    if get_customer_response.status_code != 200:
        return {
            "statusCode": get_customer_response.status_code,
            "body": json.dumps({
                "message": "Non OK response code - " + str(get_customer_response.status_code) + " - returned"
            })
        }
        
    put_order_response = put_order()

    return {
        "statusCode": put_order_response["ResponseMetadata"]["HTTPStatusCode"],
        "body": json.dumps({
            "message": "Order Successfully Created"
        })
    }


def put_order():

    dynamodb = boto3.client('dynamodb')

    order_id = str(generate_random_number(1000))
    try:
        response = dynamodb.put_item(
            TableName=TABLE_NAME,
            Item={
            'OrderId':{
                'S': order_id
            },
            'CustomerId':{
                'S': '456'
            },
            'Address':{
                'S': 'placeholder'
            }
            }
        )
    except Exception as e:
        print("PUT ORDER - EXCEPTION OCCURED: " + e)
        raise Exception("Failed to Create Order")

    return response

def get_customer(get_customer_url):
    response = requests.get(get_customer_url)
    print("Get_Customer: Making Request to - " + get_customer_url)
    print("Get_Customer: Response Code - " + str(response.status_code))

    return response

def generate_random_number(max):
    return randrange(max)